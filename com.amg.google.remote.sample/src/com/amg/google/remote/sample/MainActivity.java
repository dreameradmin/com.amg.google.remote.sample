package com.amg.google.remote.sample;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.google.tv.anymotelibrary.client.AnymoteClientService;
import com.example.google.tv.anymotelibrary.client.AnymoteClientService.ClientListener;
import com.example.google.tv.anymotelibrary.client.AnymoteSender;
import com.google.anymote.Key.Code;

public class MainActivity extends Activity implements ClientListener {

    private Button               startWebView;
    
    private Button               startLiveTV;
    private Button               channelChange;
    
    private Button               startYouTube;
    private Button               startMovie;
    private Button               sendData;

    private Button               number1;
    private Button               number2;
    private Button               number3;
    private Button               number4;
    private Button               number5;
    private Button               number6;
    private Button               number7;
    private Button               number8;
    private Button               number9;
    private Button               number0;

    private Button               searchButton;
    
    private Button               channelUp;
    private Button               channelDown;

    private Spinner              callSignSpinner;
    private TextView             selectCallsing;
    
    private AnymoteSender        anymoteSender;
    private AnymoteClientService anymoteClientService;

    private final static String  TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.activity_main);

        this.startWebView = (Button) findViewById(R.id.startWebView);
        this.startWebView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "startWebView");
                final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.dreamer.co.kr"));
                anymoteSender.sendUrl(intent.toUri(Intent.URI_INTENT_SCHEME));
            }
        });

        this.startLiveTV = (Button) findViewById(R.id.startLiveTV);
        this.startLiveTV.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "startLiveTV");
                final Intent liveTVLaunchIntent = new Intent("android.intent.action.MAIN");
                liveTVLaunchIntent.setComponent(new ComponentName("com.amg.livetv.lg", "com.amg.livetv.lg.MainActivity"));
                anymoteSender.sendIntent(liveTVLaunchIntent);
            }
        });

        this.channelChange = (Button) findViewById(R.id.changeChannel);
        this.channelChange.setEnabled(false);
        this.channelChange.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "channelChange");
                final Intent liveTVLaunchIntent = new Intent(Intent.ACTION_VIEW);
//                final Intent liveTVLaunchIntent = new Intent("android.amg.service.CHANNEL_CHANGE");
                liveTVLaunchIntent.setData(Uri.parse("channel://" + selectCallsing.getText()));
//                liveTVLaunchIntent.putExtra("callsign", "MBC");
                anymoteSender.sendIntent(liveTVLaunchIntent);
                channelChange.setEnabled(false);
            }
        });
        
        this.selectCallsing = (TextView) findViewById(R.id.selectCallsign);
        
        this.callSignSpinner = (Spinner) findViewById(R.id.callSign);
        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(getApplicationContext(), R.array.callsign, android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.callSignSpinner.setAdapter(arrayAdapter);
        this.callSignSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong) {
                Log.d(TAG, "onItemSelected");
                
                selectCallsing.setText("");
                String select = (String) callSignSpinner.getSelectedItem();
                channelChange.setEnabled(true);
                selectCallsing.setText(select);
                Log.d(TAG, "select callsign : " + select);
            }

            @Override
            public void onNothingSelected(AdapterView<?> paramAdapterView) {
                Log.d(TAG, "onNothingSelected");
            }
            
        });
        
        this.startYouTube = (Button) findViewById(R.id.startYouTube);
        this.startYouTube.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "startMovie");
                String url = "http://www.youtube.com/watch?feature=player_profilepage&v=TNOsB8tRmWU";
                final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube://TNOsB8tRmWU"));
                anymoteSender.sendUrl(intent.toUri(Intent.URI_INTENT_SCHEME));
            }
        });

        this.startMovie = (Button) findViewById(R.id.startMovie);
        this.startMovie.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "startMovie");
                String movieUrl = "http://intra.dreamer.co.kr/note/vod1.m3u8";
                final Intent movieLaunchIntent = new Intent();
                movieLaunchIntent.setAction("com.amg.MOVIE_PLAYER");
                movieLaunchIntent.setData(Uri.parse(movieUrl));
                anymoteSender.sendIntent(movieLaunchIntent);            }
        });

        this.sendData = (Button) findViewById(R.id.sendData);
        this.sendData.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View v) {
                Log.d(TAG, "sendData");
                EditText editText = (EditText) findViewById(R.id.editData);
                String sendText = editText.getText().toString();
                Log.d(TAG, "sendText : " + sendText);
                if (sendText != null) {
                    anymoteSender.sendData(sendText);
                }
            }
        });
        
        buttonSetOnClickListener();

        Intent intent = new Intent(MainActivity.this, AnymoteClientService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        if (anymoteClientService != null) {
            anymoteClientService.detachClientListener(this);
        }
        unbindService(serviceConnection);
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        Log.d(TAG, "onCreateOptionsMenu");
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    private void buttonSetOnClickListener() {
        
        // channel Up/Down
        this.channelUp = (Button) findViewById(R.id.channelUp);
        this.channelUp.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View paramView) {
                Log.d(TAG, "channelUp");
                anymoteSender.sendKeyPress(Code.KEYCODE_CHANNEL_UP);
            }
        });
        
        this.channelDown = (Button) findViewById(R.id.channelDown);
        this.channelDown.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View paramView) {
                Log.d(TAG, "channelDown");
                anymoteSender.sendKeyPress(Code.KEYCODE_CHANNEL_DOWN);
            }
        });
        
        // Search button
        this.searchButton = (Button) findViewById(R.id.searchBtn);
        this.searchButton.setOnClickListener(new OnClickListener() {
            
            @Override
            public void onClick(View paramView) {
                Log.d(TAG, "search");
                anymoteSender.sendKeyPress(Code.KEYCODE_SEARCH);
            }
        });
        
        // Number button
        this.number0 = (Button) findViewById(R.id.number0);
        this.number0.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "number0");
                sendKeyEvent(KeyEvent.KEYCODE_0);
            }
        });
        this.number1 = (Button) findViewById(R.id.number1);
        this.number1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "number1");
                sendKeyEvent(KeyEvent.KEYCODE_1);
            }
        });
        this.number2 = (Button) findViewById(R.id.number2);
        this.number2.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "number2");
                sendKeyEvent(KeyEvent.KEYCODE_2);
            }
        });
        this.number3 = (Button) findViewById(R.id.number3);
        this.number3.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "number3");
                sendKeyEvent(KeyEvent.KEYCODE_3);
            }
        });
        this.number4 = (Button) findViewById(R.id.number4);
        this.number4.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "number4");
                sendKeyEvent(KeyEvent.KEYCODE_4);
            }
        });
        this.number5 = (Button) findViewById(R.id.number5);
        this.number5.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "number5");
                sendKeyEvent(KeyEvent.KEYCODE_5);
            }
        });
        this.number6 = (Button) findViewById(R.id.number6);
        this.number6.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "number6");
                sendKeyEvent(KeyEvent.KEYCODE_6);
            }
        });
        this.number7 = (Button) findViewById(R.id.number7);
        this.number7.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "number7");
                sendKeyEvent(KeyEvent.KEYCODE_7);
            }
        });
        this.number8 = (Button) findViewById(R.id.number8);
        this.number8.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "number8");
                sendKeyEvent(KeyEvent.KEYCODE_8);
            }
        });
        this.number9 = (Button) findViewById(R.id.number9);
        this.number9.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.d(TAG, "number9");
                sendKeyEvent(KeyEvent.KEYCODE_9);
            }
        });
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "onServiceDisconnected");
            anymoteClientService.detachClientListener(MainActivity.this);
            anymoteClientService = null;
        }
        
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "onServiceConnected");
            anymoteClientService = ((AnymoteClientService.AnymoteClientServiceBinder) service).getService();
            anymoteClientService.attachClientListener(MainActivity.this);
        }
    };

    private void sendKeyEvent(final int keyEvent) {
        if (anymoteSender == null) {
            Log.e(TAG, "anymoteSender is null.");
            return;
        }
        anymoteSender.sendKeyPress(keyEvent);
    }

    // ClientListener
    // ///////////////////////////////////////////////
    @Override
    public void onConnected(AnymoteSender anymoteSender) {
        Log.d(TAG, "onConnected");
        Toast.makeText(getApplicationContext(), "onConnected", Toast.LENGTH_LONG).show();
        this.anymoteSender = anymoteSender;
    }

    @Override
    public void onDisconnected() {
        Log.d(TAG, "onDisconnected");
        this.anymoteSender = null;
    }

    @Override
    public void onConnectionFailed() {
        Log.d(TAG, "onConnectionFailed");
    }

}
